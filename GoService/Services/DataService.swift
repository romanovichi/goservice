//
//  DataService.swift
//  GoService
//
//  Created by Иван Романович on 04.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class DataService {
    
    func save(order: ThreePlusOrder) {
        
        Defaults[.firstName] = order.firstName
        Defaults[.lastName] = order.lastName
        Defaults[.middleName] = order.middleName
        Defaults[.phone] = order.phone
        Defaults[.email] = order.email
    }
    
    func getOrder() -> ThreePlusOrder? {
        if Defaults[.firstName] != "" &&
            Defaults[.lastName] != "" &&
            Defaults[.middleName] != "" &&
            Defaults[.phone] != "" &&
            Defaults[.email] != "" {
            
            let personalData = ThreePlusOrder()
            personalData.firstName = Defaults[.firstName]
            personalData.lastName = Defaults[.lastName]
            personalData.middleName = Defaults[.middleName]
            personalData.phone = Defaults[.phone]
            personalData.email = Defaults[.email]
            
            return personalData
        }
        return nil
    }
    
    func gatDates() -> [ListObject]{
        
        var list = [ListObject]()
        
        let startDate = Calendar.current.date(byAdding: .year, value: -15, to: Date())
        let endDate = Calendar.current.date(byAdding: .year, value: -2, to: Date())
        var currentDate = startDate
        
        while Calendar.current.component(.year, from: currentDate!) <= Calendar.current.component(.year, from: endDate!) {
            
            list.append(ListObject(id: nil, name: String(Calendar.current.component(.year, from: currentDate!))))
            
            currentDate = Calendar.current.date(byAdding: .year, value: 1, to: currentDate!)
        }
        
        return list
    }
}
