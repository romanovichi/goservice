//
//  DefaultKeys.swift
//  GoService
//
//  Created by Иван Романович on 04.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let token = DefaultsKey<String>("token")
    static let firstName = DefaultsKey<String>("firstName")
    static let lastName = DefaultsKey<String>("lastName")
    static let middleName = DefaultsKey<String>("middleName")
    static let phone = DefaultsKey<String>("phone")
    static let email = DefaultsKey<String>("email")
}
