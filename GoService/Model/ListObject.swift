//
//  ListObject.swift
//  GoService
//
//  Created by Иван Романович on 05.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class ListObject {
    
    var id: Int?
    var name: String
    
    init(id: Int?,  name: String) {
        self.id = id
        self.name = name
    }
}
