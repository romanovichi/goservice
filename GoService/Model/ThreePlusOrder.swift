//
//  ThreePlusOrder.swift
//  GoService
//
//  Created by Иван Романович on 04.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class ThreePlusOrder {
    
    var id = 1
    var gender = 1
    var lastName = ""
    var firstName = ""
    var middleName = ""
    var email = ""
    var phone = ""
    var vin = ""
    var year = ""
    var city = 0
    var classId = 0
    var showRoomId = 0
}
