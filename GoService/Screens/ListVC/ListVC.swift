//
//  ListVC.swift
//  GoService
//
//  Created by Иван Романович on 05.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class ListVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    private var list = [ListObject]()
    var onCompleteChoose: ((ListObject)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib.init(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        tableView.tableFooterView = UIView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationBar.addNavigationBarTitle("Выберете класс")
        navigationBar.addNavigationButton(.backButton) {
            self.popViewController()
        }
    }
    
    convenience init(_ list: [ListObject]) {
        self.init()
        self.list = list
    }
}

extension ListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as? ListCell {
            cell.set(listObject: list[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        if let selectedCell = tableView.cellForRow(at: indexPath!) as? ListCell {
            onCompleteChoose?(selectedCell.getListObject())
            self.popViewController()
        }
    }
}
