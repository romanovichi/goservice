//
//  ListCell.swift
//  GoService
//
//  Created by Иван Романович on 05.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak private var idLabel: UILabel!
    @IBOutlet weak private var nameLabel: UILabel!
    
    private var listObject: ListObject! {
        didSet {
            if let id = listObject.id {
                idLabel.text = String(id)
            }
            nameLabel.text = listObject.name
        }
    }
    
    func set(listObject: ListObject) {
        self.listObject = listObject
    }
    
    func getListObject() -> ListObject {
        return listObject
    }
}
