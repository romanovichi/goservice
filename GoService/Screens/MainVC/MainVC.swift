//
//  MainVC.swift
//  GoService
//
//  Created by Иван Романович on 03.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class MainVC: BaseVC {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    
    // Text fields
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var vinTextField: UITextField!
    
    // Labels
    @IBOutlet weak var productionYearLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dealerLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    
    // Views
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var yearRowView: UIView!
    @IBOutlet weak var classRowView: UIView!
    @IBOutlet weak var cityRowView: UIView!
    @IBOutlet weak var dealerRowView: UIView!
    
    // Properties
    private let apiClient = ApiClient()
    private var dataService = DataService()
    private var threePlusOrder = ThreePlusOrder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Waiting for token
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        getToken()
        
        getOrderFromDataBase()
        
        // Setups
        setupGestureRecognizers()
        setupNavigation()
        setupObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MainVC {
    
    // MARK: Observers setup
    private func setupObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: Navigation setup
    private func setupNavigation() {
        navigationBar.addNavigationBarTitle("Service Привилегия 3+")
        navigationBar.addNavigationButton(.readyButton) {
            if self.threePlusOrderIsValid() {
                self.apiClient.postOrder(order: self.threePlusOrder, completion: { (success) in
                    if success {
                        self.showWarningMessage("Заявка отправлена!")
                        self.dataService.save(order: self.threePlusOrder)
                    } else {
                        self.showWarningMessage("Заявка не отправлена!")
                    }
                })
            }
        }
    }
    
    // MARK: Get token
    private func getToken() {
        apiClient.getToken { (gotToken) in
            if gotToken {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                self.showWarningMessage("Сервер не доступен.")
            }
        }
    }
    
    // MARK: Get last successful order data
    private func getOrderFromDataBase() {
        if let order = dataService.getOrder() {
            firstNameTextField.text! = order.firstName
            lastNameTextField.text! = order.lastName
            middleNameTextField.text! = order.middleName
            phoneTextField.text! = order.phone
            emailTextField.text! = order.email
        }
    }
    
    // MARK: Order data validation
    private func threePlusOrderIsValid() -> Bool {
        
        
        if genderSegmentedControl.selectedSegmentIndex == 0 {
            threePlusOrder.gender = 1
        } else {
            threePlusOrder.gender = 2
        }
        
        if validate(firstNameTextField), validate(lastNameTextField), validate(middleNameTextField), validate(phoneTextField), validate(emailTextField) {
            threePlusOrder.firstName = firstNameTextField.text!
            threePlusOrder.lastName = lastNameTextField.text!
            threePlusOrder.middleName = middleNameTextField.text!
            threePlusOrder.phone = phoneTextField.text!
            threePlusOrder.email = emailTextField.text!
        } else {
            showWarningMessage("Заполните все поля.")
            return false
        }
        
        if validate(vinTextField) {
            threePlusOrder.vin = vinTextField.text!
        } else {
            showWarningMessage("VIN должен состоять из 17 символов.")
            return false
        }
        
        if productionYearLabel.text == "Выберите год выпуска" {
            showWarningMessage("Выберите год.")
            return false
        }
        
        if threePlusOrder.city == 0 {
            showWarningMessage("Выберите город.")
            return false
        }
        
        if threePlusOrder.classId == 0 {
            showWarningMessage("Выберите класс.")
            return false
        }
        
        if threePlusOrder.showRoomId == 0 {
            showWarningMessage("Выберите дилера.")
            return false
        }
        return true
    }
    
    // MARK: Gesture recognizer setup
    private func setupGestureRecognizers() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        
        let tapOnDateView = UITapGestureRecognizer(target: self, action: #selector(tappedOnDateView))
        yearRowView.addGestureRecognizer(tapOnDateView)
        
        let tapOnClassView = UITapGestureRecognizer(target: self, action: #selector(tappedOnClassView))
        classRowView.addGestureRecognizer(tapOnClassView)
        
        let tapOnCityView = UITapGestureRecognizer(target: self, action: #selector(tappedOnCityView))
        cityRowView.addGestureRecognizer(tapOnCityView)
        
        let tapOnDealerView = UITapGestureRecognizer(target: self, action: #selector(tappedOnDealerView))
        dealerRowView.addGestureRecognizer(tapOnDealerView)
    }
    
    @objc func dismissKeyboard() {
        scrollView.endEditing(true)
    }
    
    @objc private func tappedOnDateView() {
        
        let list = dataService.gatDates()
        
        let listVC = ListVC(list)
        self.navigationController?.pushViewController(listVC, animated: true)
        
        listVC.onCompleteChoose = { [unowned self] yearObject in
            self.productionYearLabel.text = yearObject.name
            self.threePlusOrder.year = yearObject.name
        }
        
    }
    
    @objc private func tappedOnClassView() {
        dismissKeyboard()
        apiClient.getClassesId { (list) in
            let listVC = ListVC(list)
            self.navigationController?.pushViewController(listVC, animated: true)
            
            listVC.onCompleteChoose = { [unowned self] classObject in
                self.classLabel.text = classObject.name
                if let id = classObject.id {
                    self.threePlusOrder.classId = id
                }
            }
        }
    }
    
    @objc private func tappedOnCityView() {
        dismissKeyboard()
        apiClient.getCities { (list) in
            let listVC = ListVC(list)
            self.navigationController?.pushViewController(listVC, animated: true)
            
            listVC.onCompleteChoose = { [unowned self] cityObject in
                self.cityLabel.text = cityObject.name
                if let id = cityObject.id {
                    self.threePlusOrder.city = id
                }
            }
        }
    }
    
    @objc private func tappedOnDealerView() {
        dismissKeyboard()
        if threePlusOrder.city != 0 {
            apiClient.getDealers(cityId: threePlusOrder.city) { (list) in
                let listVC = ListVC(list)
                self.navigationController?.pushViewController(listVC, animated: true)
                
                listVC.onCompleteChoose = { [unowned self] dealerObject in
                    self.dealerLabel.text = dealerObject.name
                    if let id = dealerObject.id {
                        self.threePlusOrder.showRoomId = id
                    }
                }
            }
        } else {
            showWarningMessage("Сначала нужно выбрать город.")
        }
    }
}


// MARK: Helper functions

extension MainVC: UITextFieldDelegate {
    
    private func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Validation func
    private func validate(_ textField: UITextField) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if text.count == 0 {
            return false
        }
        
        if textField == vinTextField {
            if text.count == 17 {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    // MARK: Show warning message func
    private func showWarningMessage(_ message: String) {
        
        warningView.layer.zPosition = 1
        warningLabel.text = "\(message)\n"
        
        UIView.animate(withDuration: 0.5, animations: {
            self.warningView.alpha = 1
            self.view.layoutIfNeeded()
        }) { _ in
            UIView.animate(withDuration: 0.5, delay: 3, animations:  {
                self.warningView.alpha = 0
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, to: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
}
