//
//  BaseVC.swift
//  GoService
//
//  Created by Иван Романович on 03.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    @IBOutlet weak var navigationBar: CustomNavigationView!
    var activityIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .white
        view.addSubview(activityIndicator)
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
}
