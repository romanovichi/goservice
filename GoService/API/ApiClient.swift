//
//  ApiClient.swift
//  GoService
//
//  Created by Иван Романович on 04.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

class ApiClient {
    
    let baseUrlString = "http://3plus-authless.test.intravision.ru/api"
    let tokenUrlString = "http://identity-server.test.intravision.ru/core/connect/token"
    
    var baseUrl: URL {
        return URL(string: baseUrlString)!
    }
    
    var tokenUrl: URL {
        return URL(string: tokenUrlString)!
    }
    
    func getToken(completion: @escaping (Bool)->()) {
        
        var headers: HTTPHeaders = [:]
        headers["Authorization"] = "Basic Q3VzdG9tR3JhbnRUeXBlQ2xpZW50SWQ6Q3VzdG9tR3JhbnRUeXBlQ2xpZW50U2VjcmV0"
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        var params: Parameters = [:]
        params["grant_type"] = "custom_client_credentials"
        params["scope"] = "profile"
            
        Alamofire.request(tokenUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case let .success(result):
                if let json = result as? [String : Any],
                    let token = json["access_token"] as? String {
                    Defaults[.token] = token
                    
                }
            case let .failure(error):
                print(error)
            }
            completion(!Defaults[.token].isEmpty)
        }
    }
    
    func getClassesId(completion: @escaping ([ListObject])->()) {
        var headers: HTTPHeaders = [:]
        headers["Authorization"] = "Bearer " + Defaults[.token]
        headers["Accept"] = "application/json"
        headers["Content-type"] = "application/json"
            
        Alamofire.request(baseUrl.appendingPathComponent("/classes"), method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    let array = json.arrayValue
                    var classes = [ListObject]()
                    for item in array {
                        let id = item["Id"].intValue
                        let name = item["Name"].stringValue
                        classes.append(ListObject(id: id, name: name))
                    }
                    completion(classes)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getCities(completion: @escaping ([ListObject])->()) {
        var headers: HTTPHeaders = [:]
        headers["Authorization"] = "Bearer " + Defaults[.token]
        headers["Accept"] = "application/json"
        headers["Content-type"] = "application/json"
        
        Alamofire.request(baseUrl.appendingPathComponent("/cities"), method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    let array = json.arrayValue
                    var classes = [ListObject]()
                    for item in array {
                        let id = item["Id"].intValue
                        let name = item["Name"].stringValue
                        classes.append(ListObject(id: id, name: name))
                    }
                    completion(classes)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getDealers(cityId: Int, completion: @escaping ([ListObject])->()) {
        var headers: HTTPHeaders = [:]
        headers["Authorization"] = "Bearer " + Defaults[.token]
        headers["Accept"] = "application/json"
        headers["Content-type"] = "application/json"
        
        var params: Parameters = [:]
        params["CityId"] = String(cityId)
        
        Alamofire.request(baseUrl.appendingPathComponent("/showrooms"), method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    let array = json.arrayValue
                    var classes = [ListObject]()
                    for item in array {
                        let id = item["Id"].intValue
                        let name = item["Name"].stringValue
                        classes.append(ListObject(id: id, name: name))
                    }
                    completion(classes)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func postOrder(order: ThreePlusOrder, completion: @escaping (Bool)->()) {
        
        var headers: HTTPHeaders = [:]
        headers["Authorization"] = "Bearer " + Defaults[.token]
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        let params: [String : Any] = [
            "Gender": order.gender,
            "LastName": order.lastName.trimmingCharacters(in: [" "]),
            "FirstName": order.firstName.trimmingCharacters(in: [" "]),
            "MiddleName": order.middleName.trimmingCharacters(in: [" "]),
            "Email": order.email.trimmingCharacters(in: [" "]),
            "Phone": order.phone.trimmingCharacters(in: [" "]),
            "Vin": order.vin,
            "Year": order.year,
            "City": order.city,
            "ClassId": order.classId,
            "ShowRoomId": order.showRoomId
        ]
        
        Alamofire.request(baseUrl.appendingPathComponent("/worksheets"), method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseData { (response) in
            switch response.result {
            case .success:
                print("zoebis")
                completion(true)
            case let .failure(error):
                print(error)
                completion(false)
            }
        }
    }
    
}
